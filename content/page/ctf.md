---
title: CTF
subtitle: why ?
comments: false
---
few CTF attemps


- 404ctf (online), par DGSE (16/05 -> 10/06)


- hackvens (présentiel), par Advens
premier CTF, avec un ami de la licence, malheureusement aucun challenge de flag, mais très intéressantes rumps (pentest physique & CERT notamment) (07/10/2023)


- ECW (online) (fini 14/10 -> 30/10)


- DGHACK (online) (09/11 -> 24/11/2023)
