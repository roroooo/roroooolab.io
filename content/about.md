---
title: About me
subtitle: who am I ?
comments: false
---
étudiant de 19 ans en informatique   

Je souhaite poursuivre en cybersécurité, sans thématique précise pour le moment

un lien vers mon [cv](https://www.canva.com/design/DAFR2kWI30o/EOOSjWMRvFvKK5hW6pnRIw/view?utm_content=DAFR2kWI30o&utm_campaign=designshare&utm_medium=link&utm_source=homepage_design_menu) !

Merci du passage !

## abilities 
langages : 
- python(speciality)
- java 
- c 
- scala

comfortable with :
- git
- bash 
- docker

## formation

- [licence informatique](https://istic.univ-rennes1.fr/licence-informatique) - ISTIC - campus de Beaulieu - 35000 Rennes

## previous experiences

- 16/05/22 - 08/07/22 : 2 months internship at [IRISA](http://www-adopnet.irisa.fr/fr/) / INRIA research lab

